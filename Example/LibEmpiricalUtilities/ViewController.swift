//
//  ViewController.swift
//  LibEmpiricalUtilities
//
//  Created by Greg Elliott on 03/13/2016.
//  Copyright (c) 2016 Greg Elliott. All rights reserved.
//

import UIKit
import LibEmpiricalUtilities

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

@IBDesignable class HeaderLabel: LELabel {
    override func setup() {
        super.setup()
        textStyle.font = UIFont(name:"AvenirNext-DemiBold", size:20)!
        textStyle.letterSpacing = 4
        textStyle.uppercase = true
        applyStyle()
    }
}

@IBDesignable class SubHeaderLabel: LELabel {
    override func setup() {
        super.setup()
        textStyle.font = UIFont(name:"AvenirNext-Regular", size:15)!
        textStyle.letterSpacing = 3
        textStyle.uppercase = true
        applyStyle()
    }
}

@IBDesignable class TestButton: LEButton {
    override func setup() {
        super.setup()
        textStyle.backgroundColor = .blackColor()
        textStyle.color = .whiteColor()
        textStyle.font = UIFont(name: "AvenirNext-DemiBold", size: 14)!
        textStyle.lineSpacing = 0
        textStyle.uppercase = true
        textStyle.letterSpacing = 1.5
        padding = 10
        applyStyle()
    }
}

