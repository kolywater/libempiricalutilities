//
//  NSTimer+Blocks.h
//  iris
//
//  Created by Aaron Zinman on 7/8/14.
//  Copyright (c) 2014 Empirical Interfaces Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Definitions.h"

@interface NSTimer (Blocks)

+ (NSTimer *) scheduledTimerWithTimeInterval:(NSTimeInterval)interval repeats:(BOOL)repeats block:(VoidBlock)block;

- (void) scheduleOnMain;

@end
