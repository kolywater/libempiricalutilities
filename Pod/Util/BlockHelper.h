//
//  BlockHelper.h
//  Iris
//
//  Created by Aaron Zinman on 1/24/12.
//  Copyright (c) 2012, 2013 Empirical Interfaces Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Definitions.h"

@interface NSObject (PWObject)

+ (void)performAfterDelay:(NSTimeInterval)delay block:(VoidBlock)block;
- (void)performAfterDelay:(NSTimeInterval)delay block:(void (^)(void))block;
- (void)performBlockOnMainThread:(VoidBlock)block;

@end

void RunOnMain(VoidBlock block);
void RunOnMainOpt(BOOL runOnMain, VoidBlock block);
void RunInBackground(VoidBlock block);
void RunInLowPriorityBackground(VoidBlock block);
void dispatch_maybe_async(dispatch_queue_t queue, VoidBlock block);
void dispatch_maybe_sync(dispatch_queue_t queue, VoidBlock block);
void RunOnMainLater(VoidBlock block);
void RunOnMainAfterDelay(NSTimeInterval delay, VoidBlock block);
void RunInBackgroundAfterDelay(NSTimeInterval delay, VoidBlock block);
void RunOnQueueAfterDelay(dispatch_queue_t queue, NSTimeInterval delay, VoidBlock block);

dispatch_queue_t CreateHighPrioritySerialQueue(char *name);
dispatch_queue_t CreateSerialQueue(char *name);
dispatch_queue_t CreateConcurrentQueue(char *name);
BOOL IsCurrentQueue(const char *name);


/*
Use like this:
 
VoidBlock block = RecursiveBlock(^(VoidBlock recurse) {
  if(!done)
    recurse();
  else
    ...
    });
*/
VoidBlock RecursiveBlock(void (^block)(VoidBlock recurse));

extern void *QUEUE_NAME;