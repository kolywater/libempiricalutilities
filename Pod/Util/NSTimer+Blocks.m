//
//  NSTimer+Blocks.m
//  iris
//
//  Created by Aaron Zinman on 7/8/14.
//  Copyright (c) 2014 Empirical Interfaces Inc. All rights reserved.
//

#import "NSTimer+Blocks.h"
#import <objc/runtime.h>

@interface NSTimerHelper : NSObject
@property(nonatomic, strong) VoidBlock block;
- (void) _fireBlock:(NSTimer *)this;
@end

@interface NSTimer (BlocksPrivate)
@property(nonatomic, strong) NSTimerHelper *_timerHelper;
@end

@implementation NSTimer (Blocks)

+ (NSTimer *) scheduledTimerWithTimeInterval:(NSTimeInterval)interval repeats:(BOOL)repeats block:(VoidBlock)block {
  NSTimerHelper *helper = [NSTimerHelper new];
  helper.block = block;
  NSTimer *t = [NSTimer scheduledTimerWithTimeInterval:interval
                                                target:helper
                                              selector:@selector(_fireBlock:)
                                              userInfo:nil
                                               repeats:repeats];
  if ([t respondsToSelector:@selector(setTolerance:)]) {
    t.tolerance = interval * 0.1; // Recommended to 10% by apple
  }
  objc_setAssociatedObject(t, @selector(_timerHelper), helper, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  return t;
}

- (void) scheduleOnMain {
  [[NSRunLoop mainRunLoop] addTimer:self forMode:NSRunLoopCommonModes];
}

@end

@implementation NSTimerHelper

- (void) _fireBlock:(NSTimer *)this {
  if (!self.block) {
    NSLog(@"Couldn't find block to fire");
    return;
  }
  self.block();
}

@end