//
//  Definitions.h
//  Pods
//
//  Created by Greg Elliott on 3/11/16.
//
//

#ifndef Definitions_h
#define Definitions_h

typedef void (^VoidBlock)(void);
typedef void (^IdBlock)(id _);
typedef id (^IdIdBlock)(id _);

#define validString(str) (str != nil && [str length] != 0)

#endif /* Definitions_h */
