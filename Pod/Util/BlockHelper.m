//
//  BlockHelper.m
//  Stateless
//
//  Created by Aaron Zinman on 1/24/12.
//  Copyright (c) 2012 Empirical. All rights reserved.
//

#import "BlockHelper.h"

@implementation NSObject (PWObject)

+ (void)performAfterDelay:(NSTimeInterval)delay block:(VoidBlock)block {
    int64_t delta = (int64_t)(1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}

- (void)performAfterDelay:(NSTimeInterval)delay block:(VoidBlock)block {
    int64_t delta = (int64_t)(1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}

- (void)performBlockOnMainThread:(VoidBlock)block {
    dispatch_async(dispatch_get_main_queue(), block);
}

@end


void *QUEUE_NAME = &QUEUE_NAME;

inline void RunOnMain(VoidBlock block) {
  if ([NSThread isMainThread]) {
    block();
  } else {
    dispatch_async(dispatch_get_main_queue(), block);
  }
}

inline void RunOnMainLater(VoidBlock block) {
  dispatch_async(dispatch_get_main_queue(), block);
}

inline void RunOnMainAfterDelay(NSTimeInterval delay, VoidBlock block) {
  [NSObject performAfterDelay:delay block:block];
}

inline void RunOnMainOpt(BOOL runOnMain, VoidBlock block) {
  if (runOnMain) {
    if ([NSThread isMainThread]) {
      block();
    } else {
      dispatch_async(dispatch_get_main_queue(), block);
    }
  } else {
    block();
  }
}

inline void RunInBackground(VoidBlock block) {
  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
  dispatch_async(queue, block);
}

inline void RunInLowPriorityBackground(VoidBlock block) {
  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
  dispatch_async(queue, block);
}

inline void RunInBackgroundAfterDelay(NSTimeInterval delay, VoidBlock block) {
  int64_t delta = (int64_t)(1.0e9 * delay);
  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), queue, block);
}

inline void RunOnQueueAfterDelay(dispatch_queue_t queue, NSTimeInterval delay, VoidBlock block) {
  int64_t delta = (int64_t)(1.0e9 * delay);
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), queue, block);
}

inline void dispatch_maybe_async(dispatch_queue_t queue, VoidBlock block) {
  char *queueName = (char *)dispatch_queue_get_label(queue);
  if (IsCurrentQueue(queueName)) {
    block();
  } else {
    dispatch_async(queue, block);
  }
}

inline void dispatch_maybe_sync(dispatch_queue_t queue, VoidBlock block) {
  char *queueName = (char *)dispatch_queue_get_label(queue);
  if (IsCurrentQueue(queueName)) {
    block();
  } else {
    dispatch_sync(queue, block);
  }
}

dispatch_queue_t CreateSerialQueue(char *name) {
  dispatch_queue_t queue = dispatch_queue_create(name, DISPATCH_QUEUE_SERIAL);
  dispatch_queue_set_specific(queue, QUEUE_NAME, name, NULL);
  return queue;
}

dispatch_queue_t CreateHighPrioritySerialQueue(char *name) {
  dispatch_queue_attr_t attr = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL,
                                                   QOS_CLASS_USER_INTERACTIVE, 0);
  dispatch_queue_t queue = dispatch_queue_create(name, attr);
  dispatch_queue_set_specific(queue, QUEUE_NAME, name, NULL);
  return queue;
}

dispatch_queue_t CreateConcurrentQueue(char *name) {
  dispatch_queue_t queue = dispatch_queue_create(name, DISPATCH_QUEUE_CONCURRENT);
  dispatch_queue_set_specific(queue, QUEUE_NAME, name, NULL);
  return queue;
}

BOOL IsCurrentQueue(const char *name) {
  const char *queueName = dispatch_get_specific(QUEUE_NAME);
  if (queueName == NULL) {
#ifdef __IPHONE_OS_VERSION_MIN_REQUIRED
    queueName = dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL);
#else
    queueName = dispatch_queue_get_label(dispatch_get_current_queue());
#endif
  }
  
  return strcmp(queueName, name) == 0;
}

// https://gist.github.com/mikeash/1254684
VoidBlock RecursiveBlock(void (^block)(VoidBlock recurse)) {
  // assuming ARC, so no explicit copy
  return ^{ block(RecursiveBlock(block)); };
}

