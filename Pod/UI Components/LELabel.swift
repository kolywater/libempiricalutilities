//
//  UILabel+TextStyle.swift
//  Pods
//
//  Created by Greg Elliott on 3/11/16.
//
//

import Foundation

@IBDesignable public class LELabel:UILabel {
  public override init(frame:CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  public required init?(coder aDecoder:NSCoder) {
    super.init(coder:aDecoder)
    setup()
  }
  
  public override func prepareForInterfaceBuilder() {
    setup()
  }
  
  public func setup() {
    // Read in values from IB where possible
    textStyle.alignment = textAlignment
    // Override in subclasses to change textStyle properties. Call super.
  }
  
  public var textStyle:LETextStyle = LETextStyle() {
    didSet { applyStyle() }
  }
  
  // A copy of the string the user assigned. Need this because setting attrText later will
  // then set the .text property to attrStr.string, which means uppercase or text modifications
  // will be persisted. We don't want that - we always want to operate on the virgin text
  // given to use by the user
  var localText: String?
  public override var text:String? {
    didSet {
      localText = text
      applyStyle()
    }
  }
  
  public func applyStyle() {
    if localText == nil { localText = text }
    
    if let t = localText {
      attributedText = NSAttributedString(string: t, textStyle: textStyle)
    }
  }
}