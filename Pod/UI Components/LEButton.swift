//
//  UIButton+TextStyle.swift
//  Pods
//
//  Created by Greg Elliott on 3/11/16.
//
//

import Foundation

@IBDesignable public class LEButton:UIButton {
  public override init(frame:CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  public required init?(coder aDecoder:NSCoder) {
    super.init(coder:aDecoder)
    setup()
  }
  
  public var isInterfaceBuilder = false
  public override func prepareForInterfaceBuilder() {
    isInterfaceBuilder = true
    setup()
  }
  
  public func setup() {
    // Read in our title from IB if it exists
    if let ibTitle = getTitleFromInterfaceBuilder() where _title == nil {
      _title = ibTitle
    }
    // Override in subclasses to change textStyle properties. Call super.
  }
  
  func getTitleFromInterfaceBuilder() -> String? {
    if let plainTitle = titleForState(.Normal) {
      return plainTitle
    } else if let attrTitle = attributedTitleForState(.Normal)?.string {
      return attrTitle
    }
    return nil
  }
  
  public var textStyle:LETextStyle = LETextStyle() {
    didSet { applyStyle() }
  }
  
  public var textStyleHighlighted:LETextStyle? {
    didSet { applyStyle() }
  }
  
  @IBInspectable
   public var padding: CGFloat? {
    didSet {
      if let p = padding {
        contentEdgeInsets = UIEdgeInsetsMake(p, p, p, p)
      }
    }
  }
  
  var _title:String?
  public var title:String? {
    get {
      return _title
    }
    set(title) {
      _title = title
      applyStyle()
    }
  }
  
  public func applyStyle() {
    backgroundColor = textStyle.backgroundColor
    
    if isInterfaceBuilder {
      // IB can't seem to render attributed title changes, but we can approximate with color and
      // font at least.
      setTitleColor(textStyle.color, forState: .Normal)
      titleLabel?.font = textStyle.font
    } else if let t = title {
      setAttributedTitle(NSAttributedString(string: t, textStyle: textStyle), forState: .Normal)
      
      if let highlighted = textStyleHighlighted {
        setAttributedTitle(NSAttributedString(string: t, textStyle: highlighted), forState: .Highlighted)
      }
    }
  }
}