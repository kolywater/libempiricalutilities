//
//  NSAttributedString+TextStyle.swift
//  Pods
//
//  Created by Greg Elliott on 3/11/16.
//
//

import Foundation

public extension NSAttributedString {
  public convenience init(string:String, textStyle:LETextStyle = LETextStyle()) {
    var letterSpacing = textStyle.letterSpacing
    var lineSpacing = textStyle.lineSpacing
    var font = textStyle.font
    
    if textStyle.uppercase {
      letterSpacing = textStyle.letterSpacingUppercase
      lineSpacing = textStyle.lineSpacingUppercase
      font = textStyle.fontUppercase
    }
    
    var str = string
    if textStyle.uppercase { str = str.uppercaseString }
    
    let p = NSMutableParagraphStyle()
    p.alignment = textStyle.alignment
    p.lineSpacing = lineSpacing
    
    let attrs:[String:AnyObject] = [
      NSParagraphStyleAttributeName: p,
      NSKernAttributeName: letterSpacing,
      NSForegroundColorAttributeName: textStyle.color,
      NSFontAttributeName: font,
      NSUnderlineStyleAttributeName: textStyle.underline.rawValue
    ]
    
    self.init(string: str, attributes: attrs)
  }
}