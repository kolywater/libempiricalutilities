//
//  UIColor+Helpers.m
//  iris
//
//  Created by Greg Elliott on 5/14/13.
//  Copyright (c) 2013 Empirical Interfaces Inc. All rights reserved.
//

#import "UIColor+Helpers.h"

@implementation UIColor (Helpers)

+ (UIColor *) colorFromHex:(NSInteger)hex {
  return [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16))/255.0
                         green:((float)((hex & 0xFF00) >> 8))/255.0
                          blue:((float)(hex & 0xFF))/255.0 alpha:1];
}


+ (UIColor *) colorFromHex:(NSInteger)hex alpha:(float)alpha {
  return [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16))/255.0
                         green:((float)((hex & 0xFF00) >> 8))/255.0
                          blue:((float)(hex & 0xFF))/255.0 alpha:alpha];
}

+ (UIColor *) debugColorRed { return [UIColor.redColor colorWithAlphaComponent:.5]; }
+ (UIColor *) debugColorPurple { return [UIColor.purpleColor colorWithAlphaComponent:.5]; }
+ (UIColor *) debugColorBlue { return [UIColor.blueColor colorWithAlphaComponent:.5]; }

- (UIColor *) alpha:(CGFloat)alpha {
  return [self colorWithAlphaComponent:alpha];
}

- (NSUInteger) hexValue {
  CGFloat red, green, blue;
  if ([self getRed:&red green:&green blue:&blue alpha:NULL]) {
    NSUInteger redInt = (NSUInteger)(red * 255 + 0.5);
    NSUInteger greenInt = (NSUInteger)(green * 255 + 0.5);
    NSUInteger blueInt = (NSUInteger)(blue * 255 + 0.5);
    
    return (redInt << 16) | (greenInt << 8) | blueInt;
  }
  return 0;
}

@end
