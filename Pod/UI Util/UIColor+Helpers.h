//
//  UIColor+Helpers.h
//  iris
//
//  Created by Greg Elliott on 5/14/13.
//  Copyright (c) 2013 Empirical Interfaces Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Helpers)

+ (UIColor *) colorFromHex:(NSInteger)hex;
+ (UIColor *) colorFromHex:(NSInteger)hex alpha:(float)alpha;
+ (UIColor *) debugColorRed;
+ (UIColor *) debugColorPurple;
+ (UIColor *) debugColorBlue;
- (UIColor *) alpha:(CGFloat)alpha;
- (NSUInteger) hexValue;
  
@end
