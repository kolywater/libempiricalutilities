//
//  TextStyle.swift
//  LibEmpirical
//
//  Created by Greg Elliott on 9/2/15.
//  Copyright (c) 2015 Empirical Interfaces Inc. All rights reserved.
//

import Foundation

public class LETextStyle : NSObject, NSCopying {
  public var didChange:(() -> Void)?
  
  public var letterSpacing:CGFloat = 1 {
    didSet { didChange?() }
  }
  
  public var letterSpacingUppercase:CGFloat = 1 {
    didSet { didChange?() }
  }
  
  public var lineSpacing: CGFloat = 1 {
    didSet { didChange?() }
  }
  
  public var lineSpacingUppercase: CGFloat = 1 {
    didSet { didChange?() }
  }
  
  public var alignment:NSTextAlignment = .Left {
    didSet { didChange?() }
  }
  
  public var color:UIColor = .blackColor() {
    didSet { didChange?() }
  }
  
  public var backgroundColor:UIColor = .whiteColor() {
    didSet { didChange?() }
  }
  
  public var uppercase = false {
    didSet { didChange?() }
  }
  
  public var underline:NSUnderlineStyle = .StyleNone {
    didSet { didChange?() }
  }
  
  private var _font = UIFont.systemFontOfSize(12)
  public var font:UIFont {
    get { return _font }
    set(newFont) {
      _font = newFont
      didChange?()
    }
  }
  
  public var fontSize:CGFloat = 12 {
    didSet {
      if let newFont = UIFont(name:_font.fontName, size:fontSize) {
        _font = newFont
      } else {
        _font = UIFont.systemFontOfSize(fontSize)
      }
      didChange?()
    }
  }
  
  
  private var _fontUppercase = UIFont.systemFontOfSize(12)
  public var fontUppercase:UIFont {
    get { return _fontUppercase }
    set(newFont) {
      _fontUppercase = newFont
      didChange?()
    }
  }
  
  public var fontSizeUppercase:CGFloat = 12 {
    didSet {
      if let newFont = UIFont(name:_fontUppercase.fontName, size:fontSizeUppercase) {
        _fontUppercase = newFont
      } else {
        _fontUppercase = UIFont.systemFontOfSize(fontSizeUppercase)
      }
      didChange?()
    }
  }
  
  // Apparently this has to be defined because of copywithzone
  override init() {
    super.init()
  }
  
  // Allows us to text styles that are assigned to be unique objects, since we don't want
  // ui elements to share the exact text style object
  public func copyWithZone(zone: NSZone) -> AnyObject {
    return LETextStyle(font: self.font,
                       letterSpacing: self.letterSpacing,
                       lineSpacing: self.lineSpacing,
                       alignment: self.alignment,
                       color: color,
                       uppercase: self.uppercase,
                       underline: self.underline)
  }
  
  public init!(font:UIFont? = nil, letterSpacing:CGFloat = 0, lineSpacing:CGFloat = 0,
               alignment:NSTextAlignment = .Left, color:UIColor? = nil, backgroundColor:UIColor? = nil,
               uppercase:Bool = false, underline:NSUnderlineStyle = .StyleNone) {
    super.init()
    if let f = font { self.font = f }
    self.letterSpacing = letterSpacing
    self.lineSpacing = lineSpacing
    self.alignment = alignment
    self.underline = underline
    if let c = color { self.color = c }
    if let bgc = backgroundColor { self.backgroundColor = bgc }
    self.uppercase = uppercase
  }
}