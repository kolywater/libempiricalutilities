//  from: http://stackoverflow.com/questions/3272335/alignment-uiimageview-with-aspect-fit
//  UIView+FrameAdditions.m
//  iris
//
//  Created by Greg Elliott on 10/31/12.
//  Copyright (c) 2012-2013 Empirical Interfaces Inc.. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface UIView (FrameAdditions)

@property CGFloat left, right, top, bottom, width, height;
@property CGPoint origin;
@property CGSize size;

@end
