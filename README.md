# LibEmpiricalUtilities

[![CI Status](http://img.shields.io/travis/Greg Elliott/LibEmpiricalUtilities.svg?style=flat)](https://travis-ci.org/Greg Elliott/LibEmpiricalUtilities)
[![Version](https://img.shields.io/cocoapods/v/LibEmpiricalUtilities.svg?style=flat)](http://cocoapods.org/pods/LibEmpiricalUtilities)
[![License](https://img.shields.io/cocoapods/l/LibEmpiricalUtilities.svg?style=flat)](http://cocoapods.org/pods/LibEmpiricalUtilities)
[![Platform](https://img.shields.io/cocoapods/p/LibEmpiricalUtilities.svg?style=flat)](http://cocoapods.org/pods/LibEmpiricalUtilities)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LibEmpiricalUtilities is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LibEmpiricalUtilities"
```

## Authors

Greg Elliott, greg@empiric.al
Aaron Zinman, aaron@empiric.al

## Copyright

Copyright Empirical Interfaces, Inc 2016

## License

LibEmpiricalUtilities is available under the MIT license. See the LICENSE file for more info.
